var express = require('express');
var mailgun = require('mailgun-js');
var csrf = require('csurf');

var csrfProtection = csrf({ cookie: true });

var router = express.Router();

var mg = mailgun({
  apiKey: process.env.MAILGUN_API_KEY,
  domain: process.env.MAILGUN_DOMAIN
});

router.get('/order.html', csrfProtection, function(req, res) {
  res.render('order', { csrfToken: req.csrfToken() });
});

router.post('/submit-order', csrfProtection, function(req, res, next) {
  var emailMsg = [
    'Name: ' + req.body.name,
    'Email: ' + req.body.email,
    'Message: ' + req.body.message
  ].join('\n');

  var data = {
    from: 'Contact Form <postmaster@mg.majime.ca>',
    to: process.env.CONTACT_EMAIL,
    'h:Reply-To': req.body.email,
    subject: req.body.email + ' has contacted you via your website',
    text: emailMsg
  }

  mg.messages().send(data, function (err, body) {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(200).end();
    }
  });
});

module.exports = router;