$(document).ready(main);

function main() {
  console.log('order.js');
  $('#order-form').validate();
  $('#order-form').submit(function(ev) { 
    // Prevent this form submittion event from being handled by anything else
    // even the default browser events
    ev.preventDefault();
    ev.stopImmediatePropagation();

    var $form = $(this);
    if (!$form.valid()) {
      return;
    }

    // convert the user entered text of all form fields into a format
    // our server can easily understand
    var formData = $form.serialize();

    // Give an indication that we are working behind the scenes
    var submitButton = $form.find('button[type="submit"]');
    submitButton.addClass('is-loading');

    // Find all inputs, textareas and buttons within this form and disable them
    $form.find(':input').prop('disabled', true);
    
    // use jquery to send the data to our server
    $.ajax({
      method: 'POST',
      url: '/submit-order',
      data: formData, 
      success: function(data, status) {
        console.log('success', data, status);
        $form.find('.notification.is-success').removeClass('is-hidden').fadeIn();
        submitButton.removeClass('is-loading');
      }, 
      error: function(xhr, status, err) {
        console.log('error', xhr, status, err);
        $form.find('.notification.is-danger').removeClass('is-hidden').fadeIn();
        submitButton.removeClass('is-loading');
        $form.find(':input').prop('disabled', false);
      }
    });
  });
}