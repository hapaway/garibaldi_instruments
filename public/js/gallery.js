$(document).ready(main);

function main() {
  // Wrap each gallery `<img>` in a `a` tag for the featherlight gallery plugin
  var $galleryImages = $('.tile img');
  $galleryImages.each(function() {
    var $img = $(this);
    $img.wrap('<a class="gallery-link" href="' + $img.attr('src') + '"></a>');
  });

  // Initialize the featherlight gallery plugin
  $('a.gallery-link').featherlightGallery({
    previousIcon: '«',
    nextIcon: '»',
    galleryFadeIn: 300,
    openSpeed: 300
  });
}