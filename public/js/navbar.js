$(document).ready(mobilenav);

// Expanding and Collapse Navigation 
function mobilenav() {
  console.log('Hello form mobilenav!');

  // Create mobile element 
  var newDiv = document.createElement('div');
  var innerSpan = document.createElement('span');

  newDiv.className = 'nav-mobile';
  newDiv.appendChild(innerSpan);

  document.querySelector('.navbar').appendChild(newDiv);

  // Mobile nav function
  var mobileNav = document.querySelector('.nav-mobile');
  var navList = document.querySelector('.nav-list');
  mobileNav.onclick = function () {
    toggleClass(this, 'nav-mobile-open');
    toggleClass(navList, 'nav-active');
  };

  // hasClass
  function hasClass(elem, className) {
    return new RegExp (' ' + className + ' ').test(' ' + elem.className + ' ');
  }

  // toggleClass
  function toggleClass(elem, className) {
    var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
      if (hasClass(elem, className)) {
        while (newClass.indexOf(' ' + className + ' ') >= 0) {
          newClass = newClass.replace(' ' + className + ' ', ' ');
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
      } else {
          elem.className += ' ' + className; 
      }
  }
}